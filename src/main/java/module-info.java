module net.hypercube.api {
	exports net.hypercube.api.worldgenerators;
	exports net.hypercube.api.assets.mesh;
	exports net.hypercube.api.metadata;
	exports net.hypercube.api.chunks;
	exports net.hypercube.api.blocks;
	exports net.hypercube.api.interaction;
	exports net.hypercube.api.gameobjects.illumination;
	exports net.hypercube.api;
	exports net.hypercube.api.games;
	exports net.hypercube.api.assets.texture;
	exports net.hypercube.api.gameobjects.blocks;
	exports net.hypercube.api.assets.material;
	exports net.hypercube.api.gameobjects.entities;
	exports net.hypercube.api.gameobjects;

//	requires lombok;
	requires transitive org.joml;
	requires java.compiler;
}