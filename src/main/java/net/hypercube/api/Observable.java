package net.hypercube.api;

import java.util.function.Consumer;

public interface Observable<T> {
	void subscribe(Consumer<T> consumer);

	void unsubscribe(Consumer<T> consumer);

	void notifySubscribers();
}
