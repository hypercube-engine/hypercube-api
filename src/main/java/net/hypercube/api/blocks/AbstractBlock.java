package net.hypercube.api.blocks;

import java.util.HashMap;
import java.util.Map;

import org.joml.Vector3d;
import org.joml.Vector3i;

import net.hypercube.api.assets.material.Material;
import net.hypercube.api.assets.mesh.Mesh;
import net.hypercube.api.gameobjects.blocks.Block;

/**
 * A block that can be placed in the world.
 *
 * @author Christian Danscheid
 */
public class AbstractBlock implements Block {

	private Mesh mesh;
	private Material material;
	private Vector3d position;

	private final Map<String, Object> meta;

	public AbstractBlock(Mesh mesh, Material material, Vector3i position) {
		this.mesh = mesh;
		this.material = material;
		this.position = new Vector3d(position);

		this.meta = new HashMap<>();
	}

	@Override
	public Map<String, Object> getMeta() {
		return meta;
	}

	@Override
	public Mesh getMesh() {
		return mesh;
	}

	@Override
	public Material getMaterial() {
		return material;
	}

	@Override
	public Vector3d getAbsoluteTranslation() {
		Vector3d result = new Vector3d();
		position.add(mesh.getAbsoluteTranslation(), result);
		return result;
	}
}
