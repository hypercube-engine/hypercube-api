package net.hypercube.api.blocks;

import java.util.NoSuchElementException;

import org.joml.Vector3i;

import net.hypercube.api.worldgenerators.IRequiredAlias;

/**
 * An IBlockManager keeps track of which blocks are registered with the engine
 *
 * @author chris
 */
public interface IBlockManager {
	Integer registerBlock(Class<? extends AbstractBlock> clazz);

	void registerAlias(Class<? extends AbstractBlock> clazz, IRequiredAlias alias);

	Class<? extends AbstractBlock> getBlockClassById(int id);

	Class<? extends AbstractBlock> getBlockClassByAlias(IRequiredAlias alias) throws NoSuchElementException;

	AbstractBlock instantiate(Class<? extends AbstractBlock> clazz, Vector3i position);
}
