package net.hypercube.api.blocks;

import org.joml.Vector3i;

import net.hypercube.api.assets.material.Material;
import net.hypercube.api.assets.mesh.BuiltinMesh;
import net.hypercube.api.assets.mesh.BuiltinMesh.KnownMesh;

/**
 * A {@link AbstractBlock block} that has the shape of a 1x1x1m cube.
 * 
 * @author Christian Danscheid
 *
 */
public abstract class CubeBlock extends AbstractBlock {

	public CubeBlock(Material material, Vector3i position) {
		super(BuiltinMesh.of(KnownMesh.BOX), material, position);
	}
}
