package net.hypercube.api;

import net.hypercube.api.assets.material.Material;
import net.hypercube.api.assets.mesh.Mesh;
import net.hypercube.api.interaction.Translated;

/**
 * Something that can be rendered in the world
 * 
 * @author Christian Danscheid
 *
 */
public interface Renderable extends Translated {
	/**
	 * Get the mesh
	 * 
	 * @return
	 */
	Mesh getMesh();

	/**
	 * Get the material
	 * 
	 * @return
	 */
	Material getMaterial();
}
