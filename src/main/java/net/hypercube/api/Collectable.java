package net.hypercube.api;

/**
 * Everything that can be stored in inventories
 * 
 * @author Christian Danscheid
 *
 */
public interface Collectable {
	/**
	 * The name to display
	 * 
	 * @return
	 */
	String getName();

	/**
	 * The description to display
	 * 
	 * @return
	 */
	String getDescription();

	/**
	 * The image to display
	 * 
	 * @return
	 */
	String getInventoryImage();
}
