package net.hypercube.api.interaction;

import org.joml.Vector3d;

/**
 * Something that can be translated
 * 
 * @author Christian Danscheid
 *
 */
public interface Translatable extends Translated {
	/**
	 * Set translation
	 * 
	 * @param translation
	 */
	void setTranslation(Vector3d translation);
}
