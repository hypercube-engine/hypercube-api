package net.hypercube.api.interaction;

import org.joml.Quaterniond;

public interface Rotated {
	/**
	 * Get absolute rotation
	 * 
	 * Absolute rotation means that all circumstances that might affect the rotation
	 * must be taken into account.
	 * 
	 * @return
	 */
	Quaterniond getAbsoluteRotation();
}
