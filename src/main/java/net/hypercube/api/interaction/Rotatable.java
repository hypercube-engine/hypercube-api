package net.hypercube.api.interaction;

import org.joml.Quaterniond;

/**
 * Something that can be rotated
 * 
 * @author chris
 *
 */
public interface Rotatable extends Rotated {
	/**
	 * Set rotation
	 * 
	 * @param rotation
	 */
	void setRotation(Quaterniond rotation);
}
