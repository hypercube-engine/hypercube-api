package net.hypercube.api.interaction;

import org.joml.Vector3d;

/**
 * Something that can be scaled
 * 
 * @author Christian Danscheid
 *
 */
public interface Scalable extends Scaled {
	/**
	 * Set scale
	 * 
	 * @param scale
	 */
	void setScale(Vector3d scale);
}
