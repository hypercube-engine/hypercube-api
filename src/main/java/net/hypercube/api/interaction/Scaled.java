package net.hypercube.api.interaction;

import org.joml.Vector3d;

public interface Scaled {
	/**
	 * Get current absolute scale factor
	 * 
	 * Absolute scale factor means that all circumstances that might affect the
	 * scale factor must be taken into account.
	 * 
	 * @return
	 */
	Vector3d getAbsoluteScale();
}
