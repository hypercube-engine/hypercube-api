package net.hypercube.api.interaction;

import org.joml.Vector3d;

public interface Translated {
	/**
	 * Get absolute translation
	 * 
	 * Absolute translation means that all circumstances that might affect the
	 * translation must be taken into account.
	 * 
	 * @return
	 */
	Vector3d getAbsoluteTranslation();
}
