package net.hypercube.api.metadata;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Christian Danscheid
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface Name {
	public String value();
}
