package net.hypercube.api.assets.mesh;

import org.joml.Quaterniond;
import org.joml.Vector3d;

public abstract class AbstractMesh implements Mesh {
	private static final Vector3d DEFAULT_SCALE = new Vector3d(0, 0, 0);
	private static final Quaterniond DEFAULT_ROTATION = new Quaterniond();
	private static final Vector3d DEFAULT_TRANSLATION = new Vector3d(0, 0, 0);

	private Vector3d scale;
	private Quaterniond rotation;
	private Vector3d translation;

	public AbstractMesh() {
		this.scale = DEFAULT_SCALE;
		this.rotation = DEFAULT_ROTATION;
		this.translation = DEFAULT_TRANSLATION;
	}

	@Override
	public Vector3d getAbsoluteScale() {
		return new Vector3d(scale);
	}

	@Override
	public void setScale(Vector3d scale) {
		this.scale = scale;
	}

	@Override
	public Quaterniond getAbsoluteRotation() {
		return new Quaterniond(rotation);
	}

	@Override
	public void setRotation(Quaterniond rotation) {
		this.rotation = rotation;
	}

	@Override
	public Vector3d getAbsoluteTranslation() {
		return new Vector3d(translation);
	}

	@Override
	public void setTranslation(Vector3d translation) {
		this.translation = translation;
	}
}
