package net.hypercube.api.assets.mesh;

public class BuiltinMesh extends AbstractMesh {
	private KnownMesh knownMesh;

	private BuiltinMesh(KnownMesh knownMesh) {
		setMesh(knownMesh);
	}

	public static BuiltinMesh of(KnownMesh knownMesh) {
		return new BuiltinMesh(knownMesh);
	}

	public KnownMesh getMesh() {
		return knownMesh;
	}

	public void setMesh(KnownMesh knownMesh) {
		this.knownMesh = knownMesh;
	}

	public static enum KnownMesh {
		BOX;
	}
}
