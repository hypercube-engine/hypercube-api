package net.hypercube.api.assets.mesh;

import org.joml.Vector3d;

public final class ExternalMesh extends AbstractMesh {
	private String location;

	public ExternalMesh(String location) {
		super();
		setLocation(location);
	}

	public ExternalMesh(String location, Vector3d scale) {
		super();
		setLocation(location);
		setScale(scale);
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
}
