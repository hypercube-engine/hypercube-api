package net.hypercube.api.assets.mesh;

import net.hypercube.api.interaction.Rotatable;
import net.hypercube.api.interaction.Scalable;
import net.hypercube.api.interaction.Translatable;

public interface Mesh extends Rotatable, Scalable, Translatable {
}
