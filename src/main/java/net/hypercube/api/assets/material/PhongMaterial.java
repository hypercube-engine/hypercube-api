package net.hypercube.api.assets.material;

import net.hypercube.api.assets.texture.Texture2D;

public class PhongMaterial implements Material {
	private Texture2D diffuseMap;

	private Texture2D normalMap;

	private Texture2D specularMap;

	private Texture2D parallaxMap;

	private Texture2D glowMap;

	private Texture2D lightMap;

	private PhongMaterial(Builder builder) {
		this.diffuseMap = builder.diffuseMap;
		this.normalMap = builder.normalMap;
		this.specularMap = builder.specularMap;
		this.parallaxMap = builder.parallaxMap;
		this.glowMap = builder.glowMap;
		this.lightMap = builder.lightMap;
	}

	public PhongMaterial(Texture2D diffuseMap, Texture2D normalMap, Texture2D specularMap, Texture2D parallaxMap,
			Texture2D glowMap, Texture2D lightMap) {
		this.diffuseMap = diffuseMap;
		this.normalMap = normalMap;
		this.specularMap = specularMap;
		this.parallaxMap = parallaxMap;
		this.glowMap = glowMap;
		this.lightMap = lightMap;
	}

	public Texture2D getDiffuseMap() {
		return diffuseMap;
	}

	public Texture2D getNormalMap() {
		return normalMap;
	}

	public Texture2D getSpecularMap() {
		return specularMap;
	}

	public Texture2D getParallaxMap() {
		return parallaxMap;
	}

	public Texture2D getGlowMap() {
		return glowMap;
	}

	public Texture2D getLightMap() {
		return lightMap;
	}

	/**
	 * Creates builder to build {@link PhongMaterial}.
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link PhongMaterial}.
	 */
	public static final class Builder {
		private Texture2D diffuseMap;
		private Texture2D normalMap;
		private Texture2D specularMap;
		private Texture2D parallaxMap;
		private Texture2D glowMap;
		private Texture2D lightMap;

		private Builder() {
		}

		public Builder withDiffuseMap(Texture2D diffuseMap) {
			this.diffuseMap = diffuseMap;
			return this;
		}

		public Builder withNormalMap(Texture2D normalMap) {
			this.normalMap = normalMap;
			return this;
		}

		public Builder withSpecularMap(Texture2D specularMap) {
			this.specularMap = specularMap;
			return this;
		}

		public Builder withParallaxMap(Texture2D parallaxMap) {
			this.parallaxMap = parallaxMap;
			return this;
		}

		public Builder withGlowMap(Texture2D glowMap) {
			this.glowMap = glowMap;
			return this;
		}

		public Builder withLightMap(Texture2D lightMap) {
			this.lightMap = lightMap;
			return this;
		}

		public PhongMaterial build() {
			return new PhongMaterial(this);
		}
	}
}
