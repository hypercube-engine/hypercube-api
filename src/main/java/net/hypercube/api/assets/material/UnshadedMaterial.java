package net.hypercube.api.assets.material;

import net.hypercube.api.assets.texture.Texture2D;

public class UnshadedMaterial implements Material {
	private Texture2D colorMap;

	private Texture2D lightMap;

	private UnshadedMaterial(Builder builder) {
		this.colorMap = builder.colorMap;
		this.lightMap = builder.lightMap;
	}

	public UnshadedMaterial(Texture2D colorMap, Texture2D lightMap) {
		this.colorMap = colorMap;
		this.lightMap = lightMap;
	}
	
	public Texture2D getColorMap() {
		return colorMap;
	}
	
	public Texture2D getLightMap() {
		return lightMap;
	}

	/**
	 * Creates builder to build {@link UnshadedMaterial}.
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link UnshadedMaterial}.
	 */
	public static final class Builder {
		private Texture2D colorMap;
		private Texture2D lightMap;

		private Builder() {
		}

		public Builder withColorMap(Texture2D colorMap) {
			this.colorMap = colorMap;
			return this;
		}

		public Builder withLightMap(Texture2D lightMap) {
			this.lightMap = lightMap;
			return this;
		}

		public UnshadedMaterial build() {
			return new UnshadedMaterial(this);
		}
	}
}
