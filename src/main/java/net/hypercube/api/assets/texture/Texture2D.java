package net.hypercube.api.assets.texture;

public class Texture2D implements Texture {
	private String path;

	public Texture2D(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
