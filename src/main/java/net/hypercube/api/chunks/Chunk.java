package net.hypercube.api.chunks;

import java.util.Map;

import org.joml.Vector3i;

import net.hypercube.api.blocks.AbstractBlock;

/**
 * A Chunk represents a 3-dimensional array of {@link AbstractBlock}
 * 
 * @author Christian Danscheid
 *
 */
public interface Chunk {
	/**
	 * Get position of this chunk
	 * 
	 * @return Vector3d position on the chunk coordinate system (!= world coordinate
	 *         system)
	 */
	Vector3i getPosition();

	/**
	 * Get the chunks's edge length
	 * 
	 * @return int edge length
	 */
	int getEdgeLength();

	/**
	 * Get the lowest possible x/y/z component in this chunk
	 * 
	 * @return int
	 */
	int getLowEnd();

	/**
	 * Get the highest possible x/y/z component in this chunk
	 * 
	 * @return int
	 */
	int getHighEnd();

	/**
	 * Get all blocks in this chunk
	 * 
	 * @return {@link Map}&lt;{@link Vector3i}, {@link AbstractBlock}&gt;
	 */
	Map<Vector3i, AbstractBlock> getLocalBlocks();

	/**
	 * Get block within this chunk
	 * 
	 * @param localPosition The position within the chunk
	 * @return {@link AbstractBlock}
	 */
	AbstractBlock getLocalBlock(Vector3i localPosition);

	/**
	 * Get block within this chunk
	 * 
	 * @param localPosition The position within the chunk
	 * @param block         The block
	 */
	void setLocalBlock(Vector3i localPosition, AbstractBlock block);
}
