package net.hypercube.api.chunks;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import org.joml.Vector3i;

import net.hypercube.api.blocks.AbstractBlock;

public final class SimpleChunk implements Chunk {

	protected final int size;
	protected final int lowEnd;
	protected final int highEnd;

	protected final Vector3i position;

	protected final Map<Vector3i, AbstractBlock> localBlocks;

	public SimpleChunk(Vector3i position, int size) {
		if (position == null)
			throw new IllegalArgumentException("Position must not be null");

		if (size < 1)
			throw new IllegalArgumentException("Size must be an integer >= 1");

		this.position = position;
		this.size = size;
		this.lowEnd = -Math.floorDiv(size, 2);
		this.highEnd = Math.floorDiv(size, 2);

		this.localBlocks = new HashMap<>((int) Math.pow(size, 3));

		IntStream.range(lowEnd, highEnd + 1).forEach(x -> {
			IntStream.range(lowEnd, highEnd + 1).forEach(y -> {
				IntStream.range(lowEnd, highEnd + 1).forEach(z -> {
					localBlocks.put(new Vector3i(x, y, z), null);
				});
			});
		});
	}

	@Override
	public Vector3i getPosition() {
		return new Vector3i(position);
	}

	@Override
	public int getEdgeLength() {
		return size;
	}

	@Override
	public int getLowEnd() {
		return lowEnd;
	}

	@Override
	public int getHighEnd() {
		return highEnd;
	}

	@Override
	public Map<Vector3i, AbstractBlock> getLocalBlocks() {
		return localBlocks;
	}

	@Override
	public AbstractBlock getLocalBlock(Vector3i localPosition) {
		return localBlocks.get(localPosition);
	}

	@Override
	public void setLocalBlock(Vector3i localPosition, AbstractBlock block) {
		localBlocks.put(localPosition, block);
	}

	@Override
	public String toString() {
		return String.format("Chunk(%d, %d, %d)", position.x, position.y, position.z);
	}

}
