package net.hypercube.api.chunks;

import org.joml.Vector3d;
import org.joml.Vector3i;

public final class ChunkUtils {

	private ChunkUtils() {
	}

	/**
	 * Get chunk coordinates of a worldPosition
	 * 
	 * @param worldPosition The world positon
	 * @param edgeLength    The chunk's edge length
	 * @return Position of the chunk in chunk coordinates
	 */
	public static Vector3i chunkPositionFromWorldPosition(Vector3d worldPosition, int edgeLength) {
		return new Vector3i((int) Math.round(worldPosition.x / edgeLength),
				(int) Math.round(worldPosition.y / edgeLength), (int) Math.round(worldPosition.z / edgeLength));
	}

	/**
	 * Get the position of a {@link Chunk} in world coordinates.
	 * 
	 * @param chunk The chunk
	 * @return Position of the Chunk in world coordinates
	 */
	public static Vector3i worldPositionFromChunk(Chunk chunk) {
		var chunkPos = chunk.getPosition();
		var edgeLength = chunk.getEdgeLength();

		return new Vector3i(chunkPos.x * edgeLength, chunkPos.y * edgeLength, chunkPos.z * edgeLength);
	}

	/**
	 * Get the world coordinates of a position in a chunk
	 * 
	 * @param chunk         The chunk
	 * @param localPosition A position within the chunk
	 * @return Position in world coordinates
	 */
	public static Vector3i localPositionToWorldPosition(Chunk chunk, Vector3i localPosition) {
		var chunkPositionInWorld = worldPositionFromChunk(chunk);

		return chunkPositionInWorld.add(localPosition);
	}
}
