package net.hypercube.api.games;

import java.util.Set;

import net.hypercube.api.blocks.IBlockManager;
import net.hypercube.api.gameobjects.entities.Player;
import net.hypercube.api.gameobjects.illumination.Illuminating;
import net.hypercube.api.worldgenerators.IWorldGenerator;

/**
 * Represents a collection of blocks and gameplay features
 *
 * @author Christian Danscheid
 */
public interface IGame {

	/**
	 * Get game name
	 * 
	 * @return
	 */
	String getName();

	/**
	 * Register required blocks with the engine
	 * 
	 * @param blockManager The {@link IBlockManager} to use
	 */
	void registerBlocks(IBlockManager blockManager);

	/**
	 * Register the world generator with the engine
	 * 
	 * @return The {@link IWorldGenerator} to use
	 */
	IWorldGenerator getWorldGenerator(IBlockManager blockManager, Integer seed);

	/**
	 * Called whenever a player joins the game
	 * 
	 * @return The Player
	 */
	Player onPlayerJoining(String name);

	/**
	 * Get the lights to use globally in the world, usually contains at least the
	 * sun
	 * 
	 * @return
	 */
	Set<? extends Illuminating> getWorldLights();
}
