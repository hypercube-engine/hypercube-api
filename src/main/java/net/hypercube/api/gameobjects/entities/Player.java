package net.hypercube.api.gameobjects.entities;

public interface Player extends Entity {
	String getName();
}
