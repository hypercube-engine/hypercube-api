package net.hypercube.api.gameobjects.entities;

import net.hypercube.api.Observable;
import net.hypercube.api.Renderable;
import net.hypercube.api.interaction.Rotatable;
import net.hypercube.api.interaction.Scalable;
import net.hypercube.api.interaction.Translatable;

/**
 * Entities are object that can be placed and moved freely in the world
 * 
 * @author Christian Danscheid
 *
 */
public interface Entity extends Renderable, Scalable, Rotatable, Translatable, Observable<Entity> {
	/**
	 * Called every world tick
	 * 
	 * @param dtime
	 * @return
	 */
	void onStep(double dtime);
}
