package net.hypercube.api.gameobjects.illumination;

/**
 * Ambient light illuminates the whole world equally and from all angles, but
 * does not cause shadows
 * 
 * @author Christian Danscheid
 *
 */
public interface AmbientLighting extends Illuminating {
	/**
	 * Get the light intensity
	 * 
	 * @return
	 */
	float getIntensity();

	/**
	 * Set light intensity
	 * 
	 * @param intensity
	 */
	void setIntensity(float intensity);
}
