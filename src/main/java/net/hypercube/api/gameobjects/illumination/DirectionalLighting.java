package net.hypercube.api.gameobjects.illumination;

import org.joml.Vector3d;

/**
 * Directional light illuminates the world and may cause shadows
 * 
 * @author Christian Danscheid
 *
 */
public interface DirectionalLighting extends Illuminating {
	/**
	 * Get the light direction
	 * 
	 * @return
	 */
	Vector3d getDirection();

	/**
	 * Set light direction
	 * 
	 * @param direction
	 */
	void setDirection(Vector3d direction);
}
