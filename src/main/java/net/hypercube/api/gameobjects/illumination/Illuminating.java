package net.hypercube.api.gameobjects.illumination;

/**
 * Base interface for everything that illuminates the world
 * 
 * @author Christian Danscheid
 *
 */
public interface Illuminating {
	/**
	 * Determines the type of light
	 * 
	 * @return
	 */
	IlluminationType getType();
}
