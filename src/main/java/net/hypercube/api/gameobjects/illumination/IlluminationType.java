package net.hypercube.api.gameobjects.illumination;

/**
 * Different types of illumination
 * 
 * @author Christian Danscheid
 *
 */
public enum IlluminationType {
	/**
	 * Ambient light lights the whole world equally from all angles but does not
	 * cause shadows
	 */
	AMBIENT,

	/**
	 * Directional light illuminates the world and may cause shadows
	 */
	DIRECTIONAL;
}
