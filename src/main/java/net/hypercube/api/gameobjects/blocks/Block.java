package net.hypercube.api.gameobjects.blocks;

import java.util.Map;

import net.hypercube.api.Renderable;
import net.hypercube.api.gameobjects.GameObject;

/**
 * Blocks are immobile object in the world that can be placed within a 3D grid
 * 
 * @author Christian Danscheid
 *
 */
public interface Block extends GameObject, Renderable {
	Map<String, Object> getMeta();

	default void onPlace() {
	}

	default void onRemove() {
	}
}
