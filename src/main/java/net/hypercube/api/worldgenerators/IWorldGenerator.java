package net.hypercube.api.worldgenerators;

import java.util.EnumMap;
import java.util.function.Function;

import org.joml.Vector3i;

import net.hypercube.api.blocks.AbstractBlock;
import net.hypercube.api.chunks.Chunk;

/**
 *
 * @author Christian Danscheid
 */
public interface IWorldGenerator {
	Chunk generateChunk(Vector3i centerPosition);

	int getChunkEdgeLength();

	EnumMap<? extends IRequiredAlias, Function<Vector3i, ? extends AbstractBlock>> getRequiredBlockClasses();
}
